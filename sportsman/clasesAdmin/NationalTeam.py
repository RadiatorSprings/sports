from django.contrib import admin
from ..models import NationalTeam, SportsmanTournament, Tournament


class STInline(admin.TabularInline):
    extra = 0
    model = SportsmanTournament
    raw_id_fields = ("sportsman",)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "tournament":
            nt = NationalTeam.objects.get(pk=request.path.split('/')[4])
            kwargs['queryset'] = Tournament.objects.filter(sport_id=nt.sport.id)

        return super().formfield_for_foreignkey(db_field, request, **kwargs)

class NTAdmin(admin.ModelAdmin):
    list_filter = ('sport', 'country', 'gender')
    list_display = ('name', 'sport', 'country', 'gender')
    inlines = [
        STInline
    ]

    def view_on_site(self, obj):
        return '/{sport}/national-team/{gender}/'.format(sport=obj.sport.translate_name, gender=obj.gender)


admin.site.register(NationalTeam, NTAdmin)
