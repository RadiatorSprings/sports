from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin

from ..models import Tribune


class TribuneAdmin(SummernoteModelAdmin):
    list_display = ('user', 'title', 'data_publish',)
    list_filter = ('user',)
    summernote_fields = ('text',)


admin.site.register(Tribune, TribuneAdmin)
