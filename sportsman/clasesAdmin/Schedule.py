from django.contrib import admin
from ..models import Schedule, Match


class MatchInline(admin.TabularInline):
    extra = 0
    model = Match


class ScheduleAdmin(admin.ModelAdmin):
    list_display = ('id_tournament', 'year', 'gender', 'country')
    list_filter = ('year', 'gender', 'country')
    inlines = [
        MatchInline
    ]

admin.site.register(Schedule, ScheduleAdmin)
