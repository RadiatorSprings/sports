from django.contrib import admin
from ..models import Sportsman, Career, Achievements, SportsmanTournament
from django import forms


class CareerInline(admin.TabularInline):
    extra = 0
    model = Career
    raw_id_fields = ("id_club",)


class AchievementsInline(admin.TabularInline):
    extra = 0
    model = Achievements


class SportsmanTournamentInline(admin.TabularInline):
    extra = 0
    model = SportsmanTournament


class SportsmanForm(forms.ModelForm):
    class Meta:
        model = Sportsman
        fields = "__all__"


class SportsmanAdmin(admin.ModelAdmin):
    actions_on_top = True
    actions_on_bottom = True
    search_fields = ['user_id', 'last_name', 'first_name', 'middle_name']
    list_display = ('user_id', 'last_name', 'first_name', 'middle_name')
    list_filter = ('sport', 'sex', 'moderated')
    form = SportsmanForm
    inlines = [
        CareerInline,
        AchievementsInline,
        SportsmanTournamentInline
    ]

    def view_on_site(self, obj):
        return '/sportsmans/{user_id}/'.format(user_id=obj.user_id)


admin.site.register(Sportsman, SportsmanAdmin)
