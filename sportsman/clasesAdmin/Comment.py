from django.contrib import admin
from ..models import Comment


class CommentAdmin(admin.ModelAdmin):
    readonly_fields = ['date_create', 'user', 'news']
    list_display = ('news', 'date_create', 'is_checked')
    list_filter = ('date_create', 'is_checked')

admin.site.register(Comment, CommentAdmin)
