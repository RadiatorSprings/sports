from ..models import News
from django import forms
from django.contrib import admin


class NewsAdminForm(forms.ModelForm):
    class Meta:
        model = News
        fields = "__all__"


class NewsAdmin(admin.ModelAdmin):
    search_fields = ['news_id']
    form = NewsAdminForm


admin.site.register(News, NewsAdmin)

