from django.contrib import admin
from ..models import Sport


class SportAdmin(admin.ModelAdmin):
    search_fields = ['name']


admin.site.register(Sport, SportAdmin)
