from django.contrib import admin
from django import forms
from ..models import Club, Career


class CareerInline(admin.TabularInline):
    extra = 0
    model = Career
    raw_id_fields = ("id_sportsman",)


class ClubAdminForm(forms.ModelForm):
    class Meta:
        model = Club
        fields = "__all__"


class ClubAdmin(admin.ModelAdmin):
    search_fields = ['name', 'city', 'country']
    list_display = ('name', 'sport', 'gender', 'city', 'country')
    list_filter = ('sport', 'gender', 'country')
    form = ClubAdminForm
    inlines = [
        CareerInline
    ]
    # def view_on_site(self, obj):
    #     return 'http://example.com'

admin.site.register(Club, ClubAdmin)
