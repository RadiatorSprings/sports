from django.contrib.auth.models import User
from django.test import TestCase

from sportsman.models import News, Sport, Tribune


class NewsViewTest(TestCase):
    STATUS_SUCCESS = 200

    @classmethod
    def setUp(self):
        self.sport = Sport.objects.create(name='Волейбол', translate_name='volleyball')
        self.news = News.objects.create(
            kind_sport=self.sport, title='Новость', preview='Превью', text='Текст', author='Лев'
        )

    def testNewsPage(self):
        response = self.client.get('/news/{news_id}/'.format(news_id=self.news.news_id))
        self.assertEquals(self.STATUS_SUCCESS, response.status_code, 'Получили неверный код ответа')
        self.assertEquals(self.news.title, response.context['title'], 'Получили неверный title')
