from django.contrib.auth.models import User
from django.test import TestCase

from sportsman.models import Tribune


class TribuneTest(TestCase):
    @classmethod
    def setUp(self):
        self.user = User.objects.create(username='Test', email='test@test.ru', password='123456')

    def testZeroUserRead(self):
        tribune = Tribune.objects.create(user=self.user, title='Трибуна', preview='Превью', text='Текст')
        self.assertEquals(0, tribune.user_read, 'При создании трибуны, количество открытий должно быть 0')

    def testIncrementUserRead(self):
        tribune = Tribune.objects.create(user=self.user, title='Трибуна', preview='Превью', text='Текст')
        tribune.increment_user_read()
        tribune.increment_user_read()
        self.assertEquals(2, tribune.user_read, 'Неверное учитывается количество открытий трибуны')

