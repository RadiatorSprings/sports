from django.contrib.sitemaps import Sitemap

from sportsman.models import News, Sportsman, NationalTeam, Tribune


class NewsSitemap(Sitemap):
    changefreq = "never"
    priority = 0.5
    protocol = 'https'

    def items(self):
        return News.objects.all()


class SportsmanSitemap(Sitemap):
    changefreq = "monthly"
    priority = 0.8
    protocol = 'https'

    def items(self):
        return Sportsman.objects.all()


class NationalTeamSitemap(Sitemap):
    changefreq = "monthly"
    priority = 0.9
    protocol = 'https'

    def items(self):
        return NationalTeam.objects.all()


class TribuneSitemap(Sitemap):
    changefreq = "never"
    priority = 1
    protocol = 'https'

    def items(self):
        return Tribune.objects.all()