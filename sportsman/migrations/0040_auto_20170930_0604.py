# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-30 06:04
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sportsman', '0039_auto_20170930_0552'),
    ]

    operations = [
        migrations.RenameField(
            model_name='schedule',
            old_name='schedule',
            new_name='id_tournament',
        ),
    ]
