# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-07 10:05
from __future__ import unicode_literals

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sportsman', '0022_auto_20170707_1002'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='nationalteam',
            name='year',
        ),
        migrations.AddField(
            model_name='sportsmantournament',
            name='year',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.IntegerField(help_text='Нужно указывать года через запятую без пробелов', verbose_name='Года участия для отображения в клубах'), default='{0000}', help_text='Необходимо указывать год начала сезона. Указывать нужно через запятую без проблеов.', size=None),
        ),
        migrations.AlterField(
            model_name='nationalteam',
            name='gender',
            field=models.CharField(choices=[('М', 'M'), ('Ж', 'Ж')], default='М', max_length=10, verbose_name='Пол'),
        ),
    ]
