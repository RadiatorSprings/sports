# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-07 12:46
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sportsman', '0027_auto_20170707_1040'),
    ]

    operations = [
        migrations.AddField(
            model_name='sport',
            name='command',
            field=models.BooleanField(default=True, verbose_name='Командный вид спорта?'),
        ),
    ]
