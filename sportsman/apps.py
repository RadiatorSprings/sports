from django.apps import AppConfig


class SportsmanConfig(AppConfig):
    name = 'sportsman'
