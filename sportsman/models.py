import datetime
import time
from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User


class Sport(models.Model):
    name = models.CharField(max_length=50, verbose_name='Название спорта')
    command = models.BooleanField(verbose_name="Командный вид спорта?", default=True)
    translate_name = models.CharField(max_length=50, default='url', verbose_name='Название в транслите')
    description = models.TextField(blank=True, verbose_name='Информация о спорте')
    man = models.CharField(max_length=50, blank=True, verbose_name='Спортсмен мужского рода', help_text='Например, футболист')
    woman = models.CharField(max_length=50, blank=True, verbose_name='Спортсмен женского рода', help_text='Например, футболистка')
    icons = models.ImageField(
        upload_to='./static/icons/',
        verbose_name='Иконка для вида спорта',
        blank=True,
        null=True
    )

    class Meta:
        db_table = "sport"
        ordering = ['name']
        verbose_name_plural = "Виды спорта"

    def __str__(self):
        return self.name


class Club(models.Model):
    MAN = 'М'
    WOMAN = 'Ж'
    CHOICE_GENDER = (
        (MAN, 'Мужской'),
        (WOMAN, 'Женский')
    )
    name = models.CharField(max_length=100, verbose_name='Название клуба')
    logo = models.ImageField(
        upload_to='./static/image/',
        verbose_name='Логотип клуба',
        blank=True,
        null=True
    )
    sport = models.ForeignKey(Sport, verbose_name='Вид спорта', default=1, on_delete=models.CASCADE)
    gender = models.CharField(max_length=10, choices=CHOICE_GENDER, default=MAN, verbose_name="Пол")
    city = models.CharField(max_length=100, verbose_name='Город')
    country = models.CharField(max_length=100, verbose_name='Страна', default='Россия')
    site = models.CharField(max_length=100, verbose_name='Сайт', blank=True, null=True)
    create_date = models.DateField(verbose_name='Дата создания')
    closed_date = models.DateField(blank=True, null=True, verbose_name='Дата закрытия')
    about = models.TextField(max_length=50000, blank=True, null=True, verbose_name='О клубе')

    def __str__(self):
        return self.name

    class Meta:
        db_table = "club"
        verbose_name_plural = "Клубы"


class Tournament(models.Model):
    name = models.CharField(max_length=100, verbose_name='Название')
    sport = models.ForeignKey(Sport, verbose_name='Вид спорта', default=1, on_delete=models.CASCADE)
    is_visible = models.BooleanField(verbose_name="Показывать на странице спорта?", default=False)

    def __str__(self):
        return self.name

    class Meta:
        db_table = "tournament"
        verbose_name_plural = "Соревнования"


class Schedule(models.Model):
    MAN = 'man'
    WOMAN = 'woman'
    CHOICE_GENDER = (
        (MAN, 'Мужской'),
        (WOMAN, 'Женский')
    )
    id_tournament = models.ForeignKey(Tournament, verbose_name="Турнир", on_delete=models.CASCADE)
    gender = models.CharField(max_length=10, choices=CHOICE_GENDER, default=MAN, verbose_name="Пол")
    year = models.IntegerField(verbose_name='Год проведения')
    country = models.CharField(max_length=50, verbose_name='Страна проведения')

    def get_season(self):
        return str(self.year) + "/" + str(self.year + 1)

    def __str__(self):
        return str(self.id_tournament) + " " + str(self.year)

    class Meta:
        db_table = "schedule"
        verbose_name_plural = "Расписание"


class Match(models.Model):
    id_schedule = models.ForeignKey(Schedule, on_delete=models.CASCADE)
    id_club_owner = models.ForeignKey(Club, verbose_name='Клуб-хозяин', related_name='id_club_owner', on_delete=models.CASCADE)
    id_club_guest = models.ForeignKey(Club, verbose_name='Клуб-гость', related_name='id_club_guest', on_delete=models.CASCADE)
    date = models.DateTimeField(verbose_name='Дата игры', blank=True)
    score_game = models.CharField(max_length=100, verbose_name='Счет', blank=True)
    owner_set = models.IntegerField(default=0, verbose_name='Количество выиграных партий хозяином', blank=True)
    guest_set = models.IntegerField(default=0, verbose_name='Количество выиграных партий гостем', blank=True)


class Sportsman(models.Model):
    class Meta:
        db_table = "sportsman"
        verbose_name_plural = "Спортсмены"

    MAN = 'М'
    WOMAN = 'Ж'
    CHOICE_GENDER = (
        (MAN, 'Мужчина'),
        (WOMAN, 'Женщина')
    )
    user_id = models.BigIntegerField(
        verbose_name='ID пользователя',
        db_index=True,
        unique=True,
        editable=False
    )
    moderated = models.BooleanField(default=True)
    sex = models.CharField(max_length=10, choices=CHOICE_GENDER, default=MAN, verbose_name="Пол")
    sport = models.ForeignKey(Sport, verbose_name='Вид спорта', default=1, on_delete=models.CASCADE)
    last_name = models.CharField(max_length=50, verbose_name='Фамилия')
    first_name = models.CharField(max_length=50, verbose_name='Имя')
    middle_name = models.CharField(max_length=50, verbose_name='Отчество', blank=True, null=True)
    nickname = models.CharField(max_length=50, verbose_name='Прозвище', blank=True)
    instagram = models.CharField(max_length=100, verbose_name='Ник в инстаграме', blank=True)
    twitter = models.CharField(max_length=100, verbose_name='Ник в твитере', blank=True)
    role = models.CharField(max_length=100, verbose_name='Амплуа', blank=True, null=True)
    rank = models.CharField(max_length=100, verbose_name='Звание', blank=True, null=True)
    birthday = models.DateField(verbose_name='День рождения')
    height_sportsman = models.IntegerField(blank=True, null=True, verbose_name='Рост')
    died = models.DateField(blank=True, null=True, verbose_name='Дата смерти')
    start_career_player = models.IntegerField(verbose_name='Год начала карьеры спортсмена')
    end_career_player = models.IntegerField(blank=True, null=True, verbose_name='Дата окончания карьеры спортсмена')
    start_career_coach = models.IntegerField(blank=True, null=True, verbose_name='Дата начала карьеры тренера')
    end_career_coach = models.IntegerField(blank=True, null=True, verbose_name='Дата окончания крьаеры тренера')
    biography = models.TextField(max_length=50000, blank=True, verbose_name='Биография')
    popularity = models.IntegerField(default=0, verbose_name='Популярность')
    photo = models.ImageField(
        upload_to='./static/image/',
        default='static/icons/unknown.jpg',
        verbose_name='Фото'
    )

    def get_absolute_url(self):
        return "/sportsmans/{user_id}/".format(user_id=self.user_id)

    def age(self):
        return int((datetime.date.today() - self.birthday).days / 365)

    def increment_popularity(self):
        self.popularity += 1
        self.save()

    def save(self):
        if self.user_id is None:
            self.user_id = int(time.time())
        super(Sportsman, self).save()

    def __str__(self):
        return "{last_name} {first_name} {middle_name}".format(
            last_name=self.last_name,
            first_name=self.first_name,
            middle_name='' if self.middle_name is None else self.middle_name
        )


class News(models.Model):
    class Meta:
        db_table = "news"
        verbose_name_plural = "Новости"
        ordering = ['-data_publish']

    news_id = models.IntegerField(
        unique=True,
        db_index=True,
        editable=False
    )
    kind_sport = models.ForeignKey(Sport, verbose_name='Вид спорта', default=1, on_delete=models.CASCADE)
    title = models.CharField(max_length=200, verbose_name='Назывние')
    preview = models.TextField(max_length=1000, verbose_name='Анонс', blank=True, null=True)
    text = models.TextField(max_length=50000, verbose_name='Текст')
    owner = models.CharField(max_length=1000, verbose_name='Ссылка на источник', blank=True, null=True)
    data_publish = models.DateTimeField(
        verbose_name='Дата и время публикации',
        help_text="Укажите дату, когда нужно опубликовать новость"
    )
    author = models.CharField(max_length=100, verbose_name='Автор')

    def __str__(self):
        return self.title

    def save(self, **kwargs):
        if self.news_id is None:
            self.news_id = int(time.time())
        if self.data_publish is None:
            self.data_publish = timezone.now()
        super(News, self).save()

    def get_absolute_url(self):
        return "/news/{news_id}/".format(news_id=self.news_id)


class Career(models.Model):
    id_sportsman = models.ForeignKey(Sportsman, verbose_name='Спортсмен', on_delete=models.CASCADE)
    id_club = models.ForeignKey(Club, verbose_name="Клуб", on_delete=models.CASCADE)
    year_tech = ArrayField(
        models.IntegerField(verbose_name="Года участия для отображения в клубах",
                            help_text="Нужно указывать года через запятую без пробелов"),
        default=list,
        help_text="Необходимо указывать год начала сезона. Указывать нужно через запятую без проблеов."
    )
    comment = models.CharField(
        max_length=200,
        verbose_name="Комментарий",
        blank=True,
        help_text="Можно указывать внезапные переходы в середине сезонов")

    class Meta:
        db_table = "career"

    def print_year(self):
        current_season = lambda x, y: x == y or x == y + 1
        min_year = min(self.year_tech)
        max_year = max(self.year_tech)
        if current_season(int(datetime.datetime.now().year), max_year):
            item = "{start_year} - н.в. ".format(start_year=min_year)
        else:
            item = "{start_year} - {end_year} г. ".format(start_year=min_year, end_year=max_year + 1)

        return item


class Achievements(models.Model):
    id_sportsman = models.ForeignKey(Sportsman, verbose_name='Спортсмен', on_delete=models.CASCADE)
    achievement = models.CharField(max_length=100, verbose_name='Достижение')
    tournament = models.CharField(max_length=100, verbose_name='Турнир')
    year = models.CharField(max_length=90, verbose_name='Год участия', default=1970)

    class Meta:
        db_table = "achievements"


class Status(models.Model):
    name = models.CharField(max_length=100, verbose_name='Название статуса')

    class Meta:
        db_table = "status"
        verbose_name_plural = "Статусы сообщений пользователей"

    def __str__(self):
        return self.name


class Email(models.Model):
    status = models.ForeignKey(Status, verbose_name="Статус сообщения", default=1, on_delete=models.CASCADE)
    date = models.DateField(verbose_name="Дата создания")
    email = models.EmailField(verbose_name='Email')
    user_name = models.CharField(max_length=100, verbose_name='Имя пользователя')
    message = models.TextField(max_length=1000, verbose_name='Сообщение пользователя')
    user_agent = models.CharField(max_length=200, verbose_name='User agent', null=True)
    remote_address = models.CharField(max_length=20, verbose_name='IP пользователя', null=True)

    class Meta:
        db_table = "email"
        verbose_name_plural = "Сообщения пользователей"

    def __str__(self):
        return self.email


class InterestingFacts(models.Model):
    message = models.TextField(max_length=1000, verbose_name='Интересный факт')
    date = models.DateField(verbose_name="Дата создания")

    class Meta:
        db_table = "facts"
        verbose_name_plural = "Интересные факты"

    def __str__(self):
        return self.message


class NationalTeam(models.Model):
    class Meta:
        db_table = "national_team"
        verbose_name_plural = "Сборные"

    MAN = 'man'
    WOMAN = 'woman'
    CHOICE_GENDER = (
        (MAN, 'M'),
        (WOMAN, 'Ж')
    )
    photo = models.ImageField(
        upload_to='./static/image/',
        verbose_name='Фото',
        help_text="Для отображения на странице сборных",
        blank=True
    )
    name = models.CharField(max_length=1000, verbose_name='Нзвание сборной', help_text="Для отображения на странице")
    sport = models.ForeignKey(Sport, verbose_name='Вид спорта', default=1, on_delete=models.CASCADE)
    country = models.CharField(max_length=50, verbose_name='Страна')
    text = models.TextField(max_length=5000, verbose_name='О сборной')
    gender = models.CharField(max_length=10, choices=CHOICE_GENDER, default=MAN, verbose_name="Пол")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return "/{sport}/national-team/{gender}/".format(sport=self.sport.translate_name, gender=self.gender)


class SportsmanTournament(models.Model):
    national_team = models.ForeignKey(NationalTeam, on_delete=models.CASCADE)
    sportsman = models.ForeignKey(Sportsman, on_delete=models.CASCADE)
    tournament = models.ForeignKey(Tournament, default=1, on_delete=models.CASCADE)
    year = ArrayField(
        models.IntegerField(verbose_name="Года участия для отображения в клубах",
                            help_text="Нужно указывать года через запятую без пробелов"),
        default=list,
        help_text="Необходимо указывать год начала сезона. Указывать нужно через запятую без проблеов."
        )
    comment = models.TextField(blank=True, null=True, max_length=500, verbose_name='Комментарий')

    class Meta:
        db_table = "sportsman_tournament"


class Tribune(models.Model):
    class Meta:
        db_table = "tribune"
        verbose_name_plural = "Вид с трибун"
        ordering = ['-data_publish']

    user = models.ForeignKey(User, verbose_name='Ник пользователя', on_delete=models.CASCADE)
    tribune_id = models.IntegerField(unique=True, db_index=True, editable=False)
    title = models.CharField(max_length=200, verbose_name='Название')
    preview = models.TextField(max_length=1000, verbose_name='Анонс')
    text = models.TextField( verbose_name='Текст')
    data_publish = models.DateTimeField(verbose_name='Дата и время публикации')
    is_moderated = models.BooleanField(default=False)
    user_read = models.IntegerField(default=0)

    def __str__(self):
        return self.title

    def increment_user_read(self):
        self.user_read += 1
        self.save()

    def save(self, **kwargs):
        if self.tribune_id is None:
            self.tribune_id = int(time.time())
        if self.data_publish is None:
            self.data_publish = timezone.now()
        super(Tribune, self).save()

    def get_absolute_url(self):
        return "/tribune/{tribune_id}/".format(tribune_id=self.tribune_id)


class Comment(models.Model):
    user = models.ForeignKey(User, verbose_name='Ник пользователя', on_delete=models.CASCADE)
    news = models.ForeignKey(News, verbose_name='Название новости', on_delete=models.CASCADE, blank=True, null=True)
    tribune = models.ForeignKey(Tribune, verbose_name='Название трибуны', on_delete=models.CASCADE, blank=True, null=True)
    text = models.TextField(max_length=2000, verbose_name='Текст комментария')
    date_create = models.DateTimeField(editable=False, verbose_name='Дата создания')
    is_checked = models.BooleanField(default=False, verbose_name='Комментарий проверен')

    def save(self, *args, **kwargs):
        if not self.id:
            self.date_create = timezone.now()
        super(Comment, self).save()

    class Meta:
        db_table = "comments"
        verbose_name_plural = "Комментарии"
        ordering = ['-date_create']