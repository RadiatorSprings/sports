from django import forms
from django_summernote.widgets import SummernoteInplaceWidget, SummernoteWidget

from sportsman.models import Tribune


class TribuneForm(forms.ModelForm):
    class Meta:
        model = Tribune
        fields = ('title', 'preview', 'text', 'user')
        widgets = {
            'title': forms.TextInput(),
            'preview': forms.Textarea({'rows': '6'}),
            'text': SummernoteWidget(attrs={'summernote': {'width': '100%', 'height': '400px'}}),
            'user': forms.HiddenInput(),
        }

