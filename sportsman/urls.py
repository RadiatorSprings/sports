from django.conf.urls import url
from django.urls import path
from django.contrib.sitemaps.views import sitemap

from sportsman.sitemap.sitemap import TribuneSitemap, NewsSitemap, NationalTeamSitemap, SportsmanSitemap
from sportsman.views.birthday import Birthday
from sportsman.views.club.page import ClubPage
from sportsman.views.contacts import Contacts
from sportsman.views.event import EventPage
from sportsman.views.global_context import global_context
from sportsman.views.national_team.list import NationalTeamList
from sportsman.views.national_team.page import NationalTeamPage
from sportsman.views.page_not_found import PageNotFound
from sportsman.views.start_page import StartPage
from sportsman.views.news.list import NewsList
from sportsman.views.news.page import NewsPage
from sportsman.views.sport_page import SportPage
from sportsman.views.sportsman.add import AddSportsman
from sportsman.views.sportsman.list import PopularitySportsman
from sportsman.views.sportsman.page import SportsmanPage
from sportsman.views.tournament_page import TournamentPage
from sportsman.views.tribune.add import TribuneAdd
from sportsman.views.tribune.list import TribuneList
from sportsman.views.tribune.page import TribunePage
from sportsman.views.user.login import Login
from sportsman.views.user.logout import Logout
from sportsman.views.user.registration import Registration


global_context = global_context()
handler404 = PageNotFound.as_view(extra_context=global_context)

sitemaps = {
    'national_team': NationalTeamSitemap,
    'sportsman': SportsmanSitemap,
    'tribune': TribuneSitemap,
    # 'news': NewsSitemap,
}

urlpatterns = [
    # Стартовая страница
    path('', StartPage.as_view(extra_context=global_context), name='start-page'),

    # Страница авторизации/регистрации страницы
    path('login/', Login.as_view(extra_context=global_context), name='login'),
    path('registration/', Registration.as_view(), name='registration'),
    path('logout/', Logout.as_view(), name='logout'),

    # Страницы новостей
    path('news/', NewsList.as_view(extra_context=global_context), name='news-list'),
    path('news/<int:news_id>/', NewsPage.as_view(extra_context=global_context), name='news-item-page'),

    # Раздел вид с трибун
    path('tribune/', TribuneList.as_view(extra_context=global_context), name='tribune'),
    path('tribune/add/', TribuneAdd.as_view(extra_context=global_context), name='tribune-add'),
    path('tribune/<int:tribune_id>/', TribunePage.as_view(extra_context=global_context), name='tribune-page'),

    # Страницы клубов
    path('club/<int:club_id>/<str:club_name>/', ClubPage.as_view(extra_context=global_context), name='club-page'),
    path('club/<int:club_id>/<str:club_name>/<int:start_date_seasons>/', ClubPage.as_view(extra_context=global_context), name='club-page'),

    path('contacts/', Contacts.as_view(extra_context=global_context), name='contacts'),
    path('birthday/', Birthday.as_view(extra_context=global_context), name='birthday'),

    # Страница событий
    path('event/<int:event_id>/', EventPage.as_view(extra_context=global_context), name='event-page'),

    # Страницы спортсменов
    path('sportsmans/', PopularitySportsman.as_view(extra_context=global_context), name='sportsman-popularity'),
    path('sportsmans/add/', AddSportsman.as_view(extra_context=global_context), name='sportsman-add'),
    path('sportsmans/<int:user_id>/', SportsmanPage.as_view(extra_context=global_context), name='sportsman-page'),

    url(r'^[aA-zZ]+/tournaments/(?P<tournament_id>\d+)/$', TournamentPage.as_view(extra_context=global_context), name='tournament-page'),

    # # Страница сборных команд
    url(r'^[aA-zZ]+/national-team/$', NationalTeamList.as_view(extra_context=global_context), name='national-team-list'),
    url(r'^[aA-zZ]+/national-team/(?P<gender>.*)/$', NationalTeamPage.as_view(extra_context=global_context), name='national-team-page'),

    # sitemap.xml
    path('sitemap.xml', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap'),

    # # Страницы спорта
    url(r'^[aA-zZ-]+/$', SportPage.as_view(extra_context=global_context), name='sport-page'),
]
