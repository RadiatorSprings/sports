from django.shortcuts import render
from django.views.generic import TemplateView


class PageNotFound(TemplateView):
    template_name = '404.html'

    def get(self, request, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({'title': 'Потерянная страница'})
        return render(request, self.template_name, status=404)
