from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.shortcuts import render
from django.utils import timezone
from django.views.generic import TemplateView
from sportsman.models import Sport, News, Tournament


class SportPage(TemplateView):
    template_name = 'sport_page.html'

    def get(self, request, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            translate_sport_name = request.path.replace('/', '').lower()
            select_sport = Sport.objects.get(translate_name=translate_sport_name)
            news_list = News.objects.filter(kind_sport=select_sport.id).filter(data_publish__lt=timezone.now())
            paginator = Paginator(news_list, 21)
            page = request.GET.get('page', 1)
            try:
                news = paginator.page(page)
            except PageNotAnInteger:
                news = paginator.page(1)
            except EmptyPage:
                news = paginator.page(1)
            tournaments = Tournament.objects.filter(sport=select_sport.id).filter(is_visible=True)
            start_index = int(page) - 10 if int(page) > 10 else 0
            end_index = int(page) + 10
            page_range = list(paginator.page_range)[start_index:end_index]
            context.update({
                'title': select_sport,
                'select_sport': select_sport,
                'news': news,
                'page_range': page_range,
                'tournaments': tournaments,
                'description': str(select_sport) + ' - новости. Информация о спортсменах и клубах.'
            })
            return render(request, self.template_name, context)
        except Sport.DoesNotExist:
            return render(request, '404.html', {'title': 'Потерянная страничка'}, status=404)
