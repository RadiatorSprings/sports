from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render
from django.utils import timezone
from django.views.generic import TemplateView

from sportsman.models import News


class NewsList(TemplateView):
    template_name = 'news/news.html'

    def get(self, request, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        news_list = News.objects.filter(data_publish__lt=timezone.now())
        paginator = Paginator(news_list, 21)
        page = request.GET.get('page', 1)
        try:
            news = paginator.page(page)
        except PageNotAnInteger:
            news = paginator.page(1)
        except EmptyPage:
            news = paginator.page(1)
        start_index = int(page) - 10 if int(page) > 10 else 0
        end_index = int(page) + 10
        page_range = list(paginator.page_range)[start_index:end_index]
        context.update({
            'title': 'Самые свежие новости',
            'news': news,
            'page_range': page_range,
            'description': 'Спортивные новости и события'
        })
        
        return render(request, self.template_name, context)