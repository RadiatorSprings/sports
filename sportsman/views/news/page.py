from django.contrib.auth.models import User
from django.db import OperationalError
from django.shortcuts import redirect, render
from django.views.generic import TemplateView

from sportsman.models import News, Comment


class NewsPage(TemplateView):
    template_name = 'news/news_page.html'

    def get(self, request, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            news = News.objects.get(news_id=kwargs['news_id'])
            comments = Comment.objects.filter(news=news).order_by('date_create')
            context.update({
                'title': news.title,
                'news': news,
                'select_sport': news.kind_sport,
                'comments': comments,
                'description': news.preview[:150]
            })
            return render(request, self.template_name, context)
        except News.DoesNotExist:
            return render(request, '404.html', {'title': 'Потерянная страничка'}, status=404)

    def post(self, request, *args, **kwargs):
        news = News.objects.get(news_id=kwargs['news_id'])
        text = request.POST.get('text')
        user = User.objects.get(id=request.user.id)
        comment = Comment(user=user, news=news, text=text)
        try:
            comment.save()
        except OperationalError:
            pass
        return redirect('/news/' + str(news.news_id) + '/#comments')
