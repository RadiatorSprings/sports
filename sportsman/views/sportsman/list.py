from django.db.models import Q
from django.shortcuts import render
from django.views.generic import TemplateView

from sportsman.models import Sportsman


class PopularitySportsman(TemplateView):
    template_name = 'sportsmans.html'
    title = 'Страница спортсменов на sporttoday.online.'
    description = 'Поиск спортсменов и информации о них. Популярные спортсмены тридцати прошедших дней. Список популярных спортсменов портала sporttoday.online',

    def get(self, request, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        sportsmans = Sportsman.objects.filter(moderated=True).order_by('-popularity')[:45]
        context.update({
            'title': self.title,
            'description': self.description,
            'sportsmans': sportsmans,
        })
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        query = request.POST.get('query')
        if query and len(query) > 0:
            sportsmans = Sportsman.objects.\
                filter(moderated=True).\
                filter(Q(last_name__icontains=query) | Q(first_name__icontains=query))
        else:
            sportsmans = Sportsman.objects.filter(moderated=True).order_by('-popularity')[:45]

        context.update({
            'title': self.title,
            'description': self.description,
            'sportsmans': sportsmans,
        })

        return render(request, self.template_name, context)