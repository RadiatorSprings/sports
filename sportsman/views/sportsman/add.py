from django.core.exceptions import ValidationError
from django.shortcuts import render
from django.views.generic import TemplateView

from sportsman.models import Sportsman, Sport


class AddSportsman(TemplateView):
    template_name = 'sportsman_add.html'

    def get(self, request, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'title': 'Страница добавления спортсменов.',
            'description': 'Добавьте спортсмена, если не смогли его найти на sporttoday.online. Помогите сделать наш портал лучше.',
            'result': None
        })
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        context = super().get_context_data(**kwargs)

        sport = request.POST.get('sport', 1)
        last_name = request.POST.get('last_name')
        first_name = request.POST.get('first_name')
        middle_name = request.POST.get('middle_name', '')
        birthday = request.POST.get('birthday')
        role = request.POST.get('role', '')
        height_sportsman = request.POST.get('height_sportsman') or 0
        start_career_player = request.POST.get('start_career_player') or 0
        biography = request.POST.get('biography', '')
        photo = request.FILES['photo']

        if last_name and first_name and photo and birthday:
            try:
                sport = Sport.objects.get(pk=int(sport))
                sportsman = Sportsman(
                    sport=sport,
                    last_name=last_name,
                    first_name=first_name,
                    middle_name=middle_name,
                    birthday=birthday,
                    role=role,
                    height_sportsman=height_sportsman,
                    start_career_player=start_career_player,
                    biography=biography,
                    photo=photo,
                    moderated=False
                )
                sportsman.save()
                sportsmans = Sportsman.objects.filter(moderated=True).order_by('-popularity').order_by('?')[:45]
                context.update({
                    'title': 'Страница спортсменов на sporttoday.online.',
                    'description': 'Поиск спортсменов и информации о них. Популярные спортсмены тридцати прошедших дней. Список популярных спортсменов портала sporttoday.online',
                    'sportsmans': sportsmans,
                    'result': {
                        'status': 0,
                        'message': 'Ваша запись отправлена на модерацию. Спасибо, что помогаете нам улучшать проект.'
                    }
                })
                return render(request, 'sportsmans.html', context)
            except ValidationError:
                context.update({
                    'title': 'Страница добавления спортсменов.',
                    'description': 'Добавьте спортсмена, если не смогли его найти на sporttoday.online. Помогите сделать наш портал лучше.',
                    'result': {
                        'status': 1,
                        'message': 'Во время сохранения произошла ошибка. Пожалуйста, попробуйте позже.'
                    }
                })
                return render(request, self.template_name, context)