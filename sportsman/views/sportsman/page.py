from django.utils import timezone
from django.shortcuts import render
from django.views.generic import TemplateView

from sportsman.helpers.bot_detector import is_bot
from sportsman.models import Sportsman, Career, Achievements, News


class SportsmanPage(TemplateView):
    template_name = 'sportsman_page.html'

    def get(self, request, *args, **kwargs):
        context = super().get_context_data(**kwargs)

        try:
            sportsman = Sportsman.objects.get(user_id=self.kwargs['user_id'])
            career = Career.objects.filter(id_sportsman=sportsman.id).order_by('-year_tech')
            achievements = Achievements.objects.filter(id_sportsman=sportsman.id).order_by('-year')
            last_news = News.objects.filter(data_publish__lt=timezone.now())[:5]
            if not is_bot(request):
                sportsman.increment_popularity()

            context.update({
                'title': sportsman,
                'sportsman': sportsman,
                'career': career,
                'last_news': last_news,
                'achievements': achievements,
                'description': str(sportsman) + ' - информация о спортсмене, карьера, биография и его достижения в одном месте на sporttoday.online',
            })
            return render(request, self.template_name, context)
        except Sportsman.DoesNotExist:
            return render(request, '404.html', {'title': 'Потерянная страничка'}, status=404)
