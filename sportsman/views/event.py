from django.shortcuts import render
from django.views.generic import TemplateView

from sportsman.models import Schedule


class EventPage(TemplateView):
    template_name = 'event_page.html'

    def get(self, request, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            event = Schedule.objects.get(id=kwargs['event_id'])
            title = str(event.id_club_owner) + ' - ' + str(event.id_club_guest)
            context.update({
                'title': title,
                'select_sport': event.id_club_owner.sport,
                'event': event,
            })
            return render(request, self.template_name, context)
        except Schedule.DoesNotExist:
            return render(request, '404.html', {'title': 'Потерянная страничка'}, status=404)

