from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render
from django.views.generic import TemplateView

from sportsman.models import Schedule, Match


class TournamentPage(TemplateView):
    template_name = 'tournament.html'

    def get(self, request, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            schedule = Schedule.objects.filter(id_tournament=kwargs['tournament_id']).order_by('-year')
            tournament_name = schedule.first().id_tournament.name
            year = request.GET.get('year')
            if year:
                year = int(year)
                matchs = Match.objects.filter(id_schedule=schedule.get(year=year).id).order_by('date')
            else:
                matchs = Match.objects.filter(id_schedule=schedule.first().id).order_by('date')
        except ObjectDoesNotExist:
            return render(request, '404.html', {'title': 'Потерянная страница'}, status=404)

        context.update({
            'title': tournament_name,
            'tournament_name': tournament_name,
            'select_sport': schedule.first().id_tournament.sport,
            'schedule': schedule,
            'matchs': matchs,
            'year': year
        })
        return render(request, self.template_name, context)
