from django.contrib import auth
from django.core.mail import send_mail
from django.http import JsonResponse

from django.shortcuts import redirect
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.views.generic import TemplateView


class Registration(TemplateView):
    def post(self, request, *args, **kwargs):
        if not request.user.is_anonymous:
            return redirect('/')
        username = request.POST.get('username').replace(' ', '')
        email = request.POST.get('email').replace(' ', '')
        password = request.POST.get('password')
        url_redirect = request.POST.get('url-redirect')

        if self._email_exist(email):
            return JsonResponse(
                data={'error': 'Такой E-mail уже существует', 'type': 'email'},
                status=403,
                content_type='application/json'
            )
        if self._username_exist(username):
            return JsonResponse(
                data={'error': 'Такое имя пользователя уже существует', 'type': 'username'},
                status=403,
                content_type='application/json'
            )

        user = User.objects.create_user(
            username=username,
            email=email,
            password=password
        )
        user.save()
        request.session.set_expiry(604800)  # Куки ставлю на неделю
        auth.login(request, user)
        return JsonResponse({'url_redirect': url_redirect}, status=200, content_type='application/json')

    def _email_exist(self, email: str):
        try:
            User.objects.get(email=email)
            return True
        except ObjectDoesNotExist:
            return False

    def _username_exist(self, username: str):
        try:
            User.objects.get(username=username)
            return True
        except ObjectDoesNotExist:
            return False

