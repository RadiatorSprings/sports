from django.contrib import auth
from django.http import JsonResponse
from django.shortcuts import redirect, render
from django.views.generic import TemplateView


class Login(TemplateView):
    def get(self, request, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        if not request.user.is_anonymous:
            return redirect('/')

        context.update({
            'title': 'Страница авторизации пользователей',
            'message': '',
            'url_redirect': '/',
            'description': 'Страница авторизации пользователей',
        })
        return render(request, 'login_and_register.html', context)

    def post(self, request, *args, **kwargs):
        username = request.POST.get('username').replace(' ', '')
        password = request.POST.get('password')
        url_redirect = request.POST.get('url-redirect')

        user = auth.authenticate(username=username, password=password)
        if user is not None:
            request.session.set_expiry(604800)  # Куки ставлю на неделю
            auth.login(request, user)
            return JsonResponse({'url_redirect': url_redirect}, status=200, content_type='application/json')
        return JsonResponse({'error': 'Пожалуйста, введите правильные имя и пароль. Оба поля чувствительны к регистру.'}, status=403, content_type='application/json')
