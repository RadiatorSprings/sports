from django.contrib import auth
from django.shortcuts import redirect
from django.views.generic import TemplateView


class Logout(TemplateView):
    def get(self, request, *args, **kwargs):
        auth.logout(request)
        return redirect('/')