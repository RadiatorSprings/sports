from sportsman.models import Sport


def global_context():
    context = {}
    sport_list = Sport.objects.all()
    context['sport_list'] = sport_list

    return context