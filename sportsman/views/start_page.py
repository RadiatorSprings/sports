from datetime import date
from django.shortcuts import render
from django.utils import timezone
from django.views.generic import TemplateView
from sportsman.models import Sportsman, News


class StartPage(TemplateView):
    template_name = 'index.html'
    title = 'Добро пожаловать на SportToday.online - Актуальные новости спорта онлайн'
    description = 'Портал sporttoday.online расскажет о спортсменах и их карьерах в клубах и сборных. Новости спорта. Истории и статьи болельщиков.'

    def get(self, request, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        news_list = News.objects.filter(data_publish__lt=timezone.now())[:5]
        sportsman_birthday = Sportsman.objects.filter(
            birthday__day=date.today().day,
            birthday__month=date.today().month
        )
        context.update({
            'title': self.title,
            'news_list': news_list,
            'sportsman_birthday': sportsman_birthday,
            'description': self.description,
        })
        return render(request, self.template_name, context)
