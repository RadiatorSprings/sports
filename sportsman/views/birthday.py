from datetime import date

from django.shortcuts import render
from django.views.generic import TemplateView

from sportsman.models import Sportsman


class Birthday(TemplateView):
    template_name = 'birthday.html'

    def get(self, request, *args, **kwargs):
        context = super().get_context_data(**kwargs)

        sportsman_birthday = Sportsman.objects.filter(
            birthday__day=date.today().day,
            birthday__month=date.today().month
        )
        context.update({
            'title': 'Сегодня свой день рождения отмечают все известные спортсмены порталу SportToday.online',
            'sportsman_birthday': sportsman_birthday,
            'description': 'Сегодня свой день рождения отмечают все известные спортсмены порталу SportToday.online'
        })
        return render(request, self.template_name, context)
