from django.shortcuts import render
from django.views.generic import TemplateView

from sportsman.models import Sport, NationalTeam, SportsmanTournament


class NationalTeamPage(TemplateView):
    template_name = 'national_team/national_team.html'

    def get(self, request, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            select_sport = Sport.objects.get(translate_name=request.path.split('/')[1])
            nt = NationalTeam.objects.filter(sport_id=select_sport.id).get(gender=kwargs['gender'])
            sty = SportsmanTournament.objects.filter(national_team_id=nt.id)
            sportsman_tournament = sty.filter(national_team_id=nt.id)
            tournament = sportsman_tournament.distinct('tournament_id')

            years_list = []
            for item in sty:
                for i in item.year:
                    if i not in years_list:
                        years_list.append(i)
            years_list.sort(reverse=True)
            context.update({
                'select_sport': select_sport,
                'title': nt.name,
                'years_list': years_list,
                'nt': nt,
                'sportsman_tournament': sportsman_tournament,
                'tournament': tournament,
                'description': str(
                    select_sport) + ' - информация о сборных командах. Участие в турнирах. Составы с подробной информацией о спортсменах.'
            })
            return render(request, self.template_name, context)
        except:
            return render(request, '404.html', {'title': 'Потерянная страничка'}, status=404)
