from django.shortcuts import render
from django.views.generic import TemplateView

from sportsman.models import Sport, NationalTeam


class NationalTeamList(TemplateView):
    template_name = 'national_team/national_list.html'

    def get(self, request, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            select_sport = Sport.objects.get(translate_name=request.path.split('/')[1])
            national_team = NationalTeam.objects.filter(sport_id=select_sport.id)
            context.update({
                'select_sport': select_sport,
                'title': 'Сборные России',
                'national_team': national_team,
                'description': str(select_sport) + ' - информация о сборных командах. Участие в турнирах. Составы с подробной информацией о спортсменах.'
            })
            return render(request, self.template_name, context)
        except:
            return render(request, '404.html', {'title': 'Потерянная страничка'}, status=404)