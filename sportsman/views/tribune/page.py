from django.contrib.auth.models import User
from django.db import OperationalError
from django.shortcuts import render, redirect
from django.views.generic import TemplateView

from sportsman.helpers.bot_detector import is_bot
from sportsman.models import Tribune, News, Comment


class TribunePage(TemplateView):
    template_name = 'tribune/page.html'

    def get(self, request, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            tribune = Tribune.objects.get(tribune_id=self.kwargs['tribune_id'])
            comments = Comment.objects.filter(tribune=tribune).order_by('date_create')
            if not is_bot(request):
                tribune.increment_user_read()
            context.update({
                'title': tribune.title,
                'tribune': tribune,
                'comments': comments,
                'description': tribune.preview[:150] + ' Истории о спорте на sporttoday.online',
            })
            render_response = render(request, self.template_name, context)
        except Tribune.DoesNotExist:
            context.update({'title': 'Потерянная страничка'})
            render_response = render(request, '404.html', {'title': 'Потерянная страничка'}, status=404)

        return render_response

    def post(self, request, *args, **kwargs):
        tribune = Tribune.objects.get(tribune_id=kwargs['tribune_id'])
        text = request.POST.get('text')
        user = User.objects.get(id=request.user.id)
        comment = Comment(user=user, tribune=tribune, text=text)
        try:
            comment.save()
        except OperationalError:
            pass
        return redirect('/tribune/' + str(tribune.tribune_id) + '/#comments')
