from django.shortcuts import render, redirect
from django.views.generic import TemplateView

from sportsman.forms import TribuneForm


class TribuneAdd(TemplateView):
    form = TribuneForm

    def get(self, request, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        if request.user.is_anonymous:
            context.update({
                'title': 'Страница авторизации пользователей',
                'message': 'Для того чтобы добавить статью необходимо авторизоваться или пройти регистрацию',
                'url_redirect': '/tribune/add/',
                'description': 'Страница авторизации пользователей',
            })
            render_response = render(request, 'login_and_register.html', context)
        else:
            form = self.form(initial={
                'user': request.user
            })
            context.update({
                'title': 'Статьи и истории о спорте, о спортсменах, о нас',
                'form': form,
                'description': 'Страница для добавления статей и историй',
            })
            render_response = render(request, 'tribune/tribune_add.html', context)

        return render_response

    def post(self, request, *args, **kwargs):
        self.template_name = 'tribune/tribune_add.html'
        form = self.form(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/tribune/')
        else:
            context = super().get_context_data(**kwargs)
            context.update({
                'title': 'Статьи и истории о спорте, о спортсменах, о нас',
                'form': form,
                'description': 'Страница для добавления статей и историй',
            })
            return render(request, 'tribune/tribune_add.html', context)



