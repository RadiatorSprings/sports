from django.views.generic import TemplateView
from sportsman.models import Tribune


class TribuneList(TemplateView):
    template_name = 'tribune/tribune.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        tribune = Tribune.objects.filter(is_moderated=True)
        context.update({
           'title': 'Статьи и истории о спорте, о спортсменах, о нас',
           'tribune': tribune,
           'description': 'Вид с трибун всегда другой, не такой как на поле. Читайте и делитесь своими историями и размышлениями на портале sporttoday.online',
        })

        return context
