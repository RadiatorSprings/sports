from datetime import date

from django.shortcuts import render
from django.views.generic import TemplateView

from sportsman.models import Club, Career


class ClubPage(TemplateView):
    template_name = './clubs/club_page.html'

    def get(self, request, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        MONTH_JUNE = 6
        club = Club.objects.filter(name=self.kwargs['club_name']).get(id=self.kwargs['club_id'])
        careers = Career.objects.filter(id_club=club.id)

        if (club.closed_date):
            end_date = club.closed_date.year
        else:
            if date.today().month > MONTH_JUNE:
                end_date = date.today().year + 1
            else:
                end_date = date.today().year

        # Установил костыль для решения проблемы тех клубов у которых указан в базе год создания
        # 1 год от нашэй эры, например клуб под id = 282
        start_date = club.create_date.year
        if start_date < 1970:
            start_date = 1970

        seasons = sorted({value for value in range(start_date, end_date)}, reverse=True)
        context.update({
            'title': 'Клуб ' + club.name + ', ' + club.country + ', ' + club.city,
            'club': club,
            'careers': careers,
            'seasons': seasons,
            'description': str(club) + ' - информация о клубе и о его составах.'
        })
        return render(request, self.template_name, context)