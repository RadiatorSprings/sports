from datetime import date

from django.db import IntegrityError
from django.shortcuts import redirect
from django.views.generic import TemplateView

from sportsman.models import Email


class Contacts(TemplateView):
    template_name = 'contacts.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'title': 'Контакты для связи с SportToday.online',
            'description': 'Страница жалоб и предложений. Мы рады любой обратной связи, главное чтобы конструктивно и по делу.'
        })
        return context

    def post(self, request, *args, **kwargs):
        email = request.POST.get('email')
        user_name = request.POST.get('user_name')
        message = request.POST.get('message')
        user_agent = request.META.get('HTTP_USER_AGENT')
        remote_addr = request.META.get('REMOTE_ADDR')
        message_qs = Email(
            email=email,
            date=date.today(),
            user_name=user_name,
            message=message,
            user_agent=user_agent,
            remote_address=remote_addr
        )
        resp = redirect('/contacts/')
        try:
            message_qs.save()
            resp.set_cookie('success', 'true', max_age=60)
        except IntegrityError:
            resp.set_cookie('success', 'false', max_age=60)

        return resp