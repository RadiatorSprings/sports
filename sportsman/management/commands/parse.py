import random
import datetime
from time import sleep

from django.core.management import BaseCommand
from bs4 import BeautifulSoup
import requests

from sportsman.models import News, Sport


class Command(BaseCommand):
    host = ''

    '''
        pk_sport -> uri
        3 -> Футбол
        4 -> Хоккей 
        6 -> MMA 
        2 -> Баскетбол 
    '''
    sport_uri = {
        '3': 'https://www.gazeta.ru/news/sport/theme/3403.shtml',
        '4': 'https://www.gazeta.ru/news/sport/theme/3404.shtml',
        '6': 'https://www.gazeta.ru/news/sport/theme/147487.shtml',
        '2': 'https://www.gazeta.ru/news/sport/theme/3407.shtml',
    }
    headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36'
    }

    def handle(self, *args, **options):
        for sport_key, url in self.sport_uri.items():
            manager = Manager(url, self.headers)
            news = manager.run()
            kind_sport = Sport.objects.get(pk=int(sport_key))
            for news_item in news:
                date = datetime.datetime.now() + datetime.timedelta(hours=random.randint(0, 6),
                                                                    minutes=random.randint(0, 25))
                db_news = News(
                    kind_sport=kind_sport,
                    title=news_item['title'],
                    preview=news_item['preview'][:250],
                    text=news_item['text'],
                    owner=news_item['url'],
                    data_publish=date,
                    author='<a href="//www.gazeta.ru" target="_blank">www.Gazeta.ru</a>'
                )
                db_news.save()
                print(str(datetime.datetime.now()) + ' --- Done -> ' + news_item['url'])
                sleep(1)


class Manager:
    def __init__(self, url: str, headers: dict):
        self.list_news = []
        self._url = url
        self._headers = headers

    def run(self):
        response = requests.get(self._url, self._headers)
        if response.status_code < 400:
            self._parse_news(response)
            return self.list_news
        else:
            print('Получили ответ ->' + str(response.status_code))

    def _parse_news(self, response):
        soup = BeautifulSoup(response.content, "html5lib")
        blocks_news = soup.find('div', {'id': 'news_main_zone'}).find_all('article')

        for block in blocks_news:
            text = ''
            preview = ''
            try:
                News.objects.get(owner=block.get('data-url'))
                print(str(datetime.now()) + ' --- Skip -> ' + block.get('data-url'))
            except:
                response_news_page = requests.get(block.get('data-url').replace('//', 'https://'), self._headers)
                soup_news_page = BeautifulSoup(response_news_page.content, "html5lib")
                article_text = soup_news_page.find('div', {'id': 'news-content'}).find('article', {'class': 'article-text'}).find('div', {'class': 'article-text-body'})
                if article_text is not None:
                    p = article_text.find_all('p')
                    if p is not None:
                        for i in p:
                            if i.a is None:
                                preview += i.text
                                text += str(i)
                            elif i.a.get('href').find('instagram') == -1 and\
                                    i.a.get('href').find('/t.co/') == -1 and\
                                    i.a.get('href').find('twitter') == -1:
                                i.a.unwrap() # Вырезаем тег а
                                preview += i.text
                                text += str(i)
                        bq = article_text.find('blockquote')
                        if bq is not None:
                            text += str(bq)
                        iframe = article_text.find('iframe')
                        if iframe is not None:
                            text += str(iframe)
                        self.list_news.append(
                            {
                                'url': block.get('data-url'),
                                'time': block.find('div', {'class': 'news_meta'}).find('div', {'class': 'news_main_inner'}).find('time').get('datetime'),
                                'title': soup_news_page.find('h1').text,
                                'preview': preview,
                                'text': text
                            }
                        )