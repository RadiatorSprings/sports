from django.core.management import BaseCommand

from sportsman.models import Sportsman


class Command(BaseCommand):
    def handle(self, *args, **options):
        sportsmans = Sportsman.objects.filter(moderated=True).filter(popularity__gt=0)

        for sportsman in sportsmans:
            sportsman.popularity -= 1
            print('id: ' + str(sportsman.id))
            print('popularity: ' + str(sportsman.popularity))
            print('===================')
            sportsman.save()