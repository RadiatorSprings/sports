import random
import datetime
from time import sleep

import requests
from bs4 import BeautifulSoup
from django.core.management import BaseCommand

from sportsman.models import Sport, News


class Command(BaseCommand):
    host = "http://fhr.ru"
    uri = "/news/"
    id_hokey = 4
    headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36'
    }

    def handle(self, *args, **options):
        kind_sport = Sport.objects.get(pk=int(self.id_hokey))
        manager = Manager(self.host, self.uri, self.headers)
        news = manager.run()
        for news_item in news:
            db_news = News.objects.create()
            db_news.kind_sport = kind_sport
            db_news.title = news_item['title']
            db_news.preview = news_item['preview']
            db_news.text = news_item['text']
            db_news.owner = news_item['url']
            date = datetime.datetime.now() + datetime.timedelta(hours=random.randint(0, 6), minutes=random.randint(0, 25))
            db_news.data_publish = date
            db_news.author = 'Федерация Хоккея России'
            db_news.save()
            print(str(datetime.datetime.now()) + ' --- Done -> ' + news_item['url'])
            sleep(1)


class Manager:
    list_news = []

    def __init__(self, host: str, uri:str, headers: dict):
        self._host = host
        self._uri = uri
        self._headers = headers

    def run(self):
        response = requests.get(self._host + self._uri, self._headers)
        if response.status_code < 400:
            self._parse_news(response)
            return self.list_news
        else:
            print('Получили ответ ->' + str(response.status_code))

    def _parse_news(self, response):
        soup = BeautifulSoup(response.content, "html5lib")
        urls_news = soup.find_all('a', {'class': 'news-panel-img'})

        for un in urls_news:
            url = self._host + un.get('href')
            try:
                News.objects.get(owner=url)
                print(str(datetime.datetime.now()) + ' --- Skip -> ' + url)
            except:
                response = requests.get(self._host + un.get('href'), self._headers)
                soup_page = BeautifulSoup(response.content, "html5lib")
                title = soup_page.find('h1').text
                news_tags = soup_page.find('div', {'class': 'news-announce'})
                preview = news_tags.text[:news_tags.text.find('.')]

                self.list_news.append({
                    'title': title,
                    'preview': preview,
                    'text': str(news_tags),
                    'url': url
                })


