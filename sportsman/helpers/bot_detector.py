import socket
from django.core.handlers.wsgi import WSGIRequest


search_system = [
    'google',
    'yandex',
    'mail',
    'yahoo',
    'ramlber',
    'liveinternet',
    'bing',
]


def is_bot(request: WSGIRequest) -> bool:
    ip = get_user_ip(request)
    user_agent = request.META['HTTP_USER_AGENT']
    if user_agent.lower().find('bot') > -1:
        try:
            name, alias, addresslist = socket.gethostbyaddr(ip)
            return any(dns in name for dns in search_system)
        except socket.herror:
            pass


def get_user_ip(request: WSGIRequest):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip