FROM python:3

RUN apt-get update && apt-get install -y cron rsyslog

RUN mkdir -p /sporttoday

WORKDIR /sporttoday

COPY ./requirements.txt ./requirements.txt

RUN pip3 install --upgrade pip
RUN pip3 install -r requirements.txt

COPY ./ ./
COPY ./mysport/crontab/cron /etc/cron.d/cron
RUN chmod 600 /etc/cron.d/cron